{
  description = "Pod Wars";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=release-22.05";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
    cargo2nix.url = "github:cargo2nix/cargo2nix?ref=release-0.11.0";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay, cargo2nix, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        rustChannel = "stable";
        pkgs = import nixpkgs {
          inherit system;
          overlays = [
            rust-overlay.overlay
            cargo2nix.overlays.default
          ];
        };

        rustPkgs = pkgs.rustBuilder.makePackageSet {
          rustVersion = "1.61.0";
          packageFun = import ./Cargo.nix;
          extraRustComponents = [ "clippy" ];
        };

        # Dev shell rust packages from the oxalica overlay
        devShellRustPkgs = with pkgs.rust-bin.${rustChannel}.latest; [
          rustc
          cargo
          rls
          rust-analysis
        ];
      in
      rec {
        packages = {
          game = (rustPkgs.workspace.pod-wars-game { }).bin;
        };
        devShell = rustPkgs.workspaceShell
          {
            buildInputs = with pkgs;
              (devShellRustPkgs ++ [
                nixpkgs-fmt
                cargo-edit
                pre-commit
                rust-analyzer
              ]);
            RUST_SRC_PATH = "${pkgs.rust.packages.${rustChannel}.rustPlatform.rustLibSrc}";
          };
      }
    );
}
